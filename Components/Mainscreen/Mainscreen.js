import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import styles from './mainscreenStyle';

export default class Mainscreen extends Component {
  render() {
    return (
      <View style={styles.mainView}>
        <View style={styles.mainscreenStyle}>
            <Image source={require('../../Assets/images/splash_with_option.jpg')} 
                style={styles.imageStyle} />
        </View>
        <View style={styles.btnView}>
            <TouchableOpacity style={styles.btnStyle}
                onPress={()=> this.props.navigation.navigate('AudioToText')}
            >
                <Text style={styles.btnText}> Audio To Text </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnStyle}
                onPress={()=> this.props.navigation.navigate('TextToAudioContent')}
            >
                <Text style={styles.btnText}> Text To Audio </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> this.props.navigation.navigate('LanguageTranslator')}
                style={styles.btnStyle}>
                <Text style={styles.btnText}> Language Converter </Text>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}