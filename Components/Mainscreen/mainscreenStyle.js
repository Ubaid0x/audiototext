import { StyleSheet, Dimensions } from 'react-native';

export default mainscreenStyle = StyleSheet.create({
    mainView: {
        width: '100%',
        height: '100%',
        backgroundColor: '#0B4C84',
    },
    mainscreenStyle: {
        flex: 1.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageStyle: {
        width: Dimensions.get('window').width/1.8,
        height: Dimensions.get('window').height/4
    },
    btnView: {
        flex: 1,
        alignItems: 'center',
    },  
    btnStyle: {
        width: Dimensions.get('window').width/1.2,
        height: 40,
        backgroundColor: '#fff',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: Dimensions.get('window').width/75
    },
    btnText: {
        color: '#0B4C84',
        fontSize: 16
    }
})
