import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import styles from './descriptionStyle';
import Tts from 'react-native-tts';

export default class Description extends Component {
    constructor(){
        super();
        this.state = {
            text: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.'
        }
    }

    textToSpeach = ( text ) => {
        Tts.speak(text , { androidParams: { KEY_PARAM_PAN: -1, KEY_PARAM_VOLUME: 0.5, KEY_PARAM_STREAM: 'STREAM_MUSIC' } })
    }

  render() {
    return (
      <View style={styles.mainView}>
        <View style={styles.detailView}> 
            <Text style={styles.textStyle}> My Current History </Text>
            <View style={styles.descriptionView}>
                <Text>  
                    {this.state.text}
                </Text>
            </View>
            <View style={styles.btnView}>
                <TouchableOpacity style={styles.btn} onPress={()=> Tts.speak('Hello, world!', { androidParams: { KEY_PARAM_PAN: -1, KEY_PARAM_VOLUME: 0.5, KEY_PARAM_STREAM: 'STREAM_MUSIC' } })}>
                    <Text style={styles.btnText}>Translate It</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn} onPress={()=> this.textToSpeach(this.state.text)}>
                    <Text style={styles.btnText}>Convert To Audio</Text>
                </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  }
}