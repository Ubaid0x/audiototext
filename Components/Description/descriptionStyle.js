import { StyleSheet, Dimensions } from 'react-native';

export default styles = StyleSheet.create({
    mainView: {
        
    },
    detailView: {
        paddingLeft: Dimensions.get('window').width/20
    },
    textStyle: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    descriptionView: {
        width: Dimensions.get('window').width/1.1,
        height: Dimensions.get('window').height/1.35,
        backgroundColor: '#D3D3D3'
    },
    btnView: {
        flexDirection: 'row',
    },
    btn: {
        borderWidth: 1,
        borderRadius: 5,
        height: 40,
        width: 160,
        marginTop: 4,
        marginRight: 4,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0080ff' 
    },
    btnText: {
        color: 'white'
    },
})