import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Picker, TextInput } from 'react-native';
import styles from './textToAudioContentStyle';

export default class TextToAudioContent extends Component {
    constructor() {
        super();
        this.state = {
            language: ''
        }
    }

    render() {
        return (
            <View>
                <View style={styles.pickerView}>
                    <Picker
                        selectedValue={this.state.language}
                        style={{ height: 50, width: 200 }}
                        onValueChange={(itemValue, itemIndex) => this.setState({ language: itemValue })}>
                        <Picker.Item label="Select Language" value="" />
                        <Picker.Item label="English" value="English" />
                        <Picker.Item label="Urdu" value="Urdu" />
                        <Picker.Item label="Hindi" value="Hindi" />
                        <Picker.Item label="Chinese" value="Chinese" />
                        <Picker.Item label="Russian" value="Russian" />
                    </Picker>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('TextToAudio')}
                        style={styles.translateBtn}>
                        <Text> Translate It </Text>    
                    </TouchableOpacity>
                </View>
                <View style={styles.textareaView}>
                    <TextInput 
                        multiline={true}
                        placeholder='Write something here ...'
                        style={styles.textInput}
                    />
                </View>
            </View>
        )
    }
}