import { StyleSheet, Dimensions } from 'react-native';

const {width, height} = Dimensions.get('window');

export default styles = StyleSheet.create({
    pickerView: {
        flexDirection: 'row',
        borderRadius: 5,
        elevation: 1,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        marginHorizontal: width/22, 
        marginBottom: 5,
    },
    translateBtn: {
        backgroundColor: '#00BFFF',
        height: 35,
        width: 110,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    textareaView: {
        borderRadius: 5,
        elevation: 1,
        marginHorizontal: width/22,
        height: 180
    },
    textInput: {
        padding: 5,
    }
})