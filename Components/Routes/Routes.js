import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native'; 
import { createStackNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';
import Mainscreen from '../Mainscreen/Mainscreen';
import AudioToText from '../AudioToText/AudioToText';
import TextToAudio from '../TextToAudio/TextToAudio';
import Description from '../Description/Description';
import TextToAudioContent from '../TextToAudioContent/TextToAudioContent';
import LanguageTranslator from '../LanguageTranslator/LanguageTranslator';
import DrawerComponent from '../Drawer/Drawer';
import Icon from 'react-native-vector-icons/Entypo';
import AudioList from '../AudioList/AudioList';
import Flatlist from '../FlatList/FlatList';
import styles from './routeStyle';

const stackRouter = createStackNavigator({
    Mainscreen: {
        screen: Mainscreen,
        navigationOptions: {
            header: null
        }
    },
    AudioToText: {
        screen: AudioToText,
        navigationOptions: ({navigation})=>{
            return {
                headerLeft: null,
                headerRight: <TouchableOpacity onPress={() => navigation.toggleDrawer()} >
                                <Icon name="dots-three-vertical" size={30} color={'white'}/>
                            </TouchableOpacity>
            }
        }
    },
    TextToAudio: {
        screen: TextToAudio,
        navigationOptions: ({navigation}) => {
            return {
                headerStyle: {
                    backgroundColor: 'white',
                    elevation: 0
                },
                headerTitle:<View style={styles.titleTextView}>
                                <Text style={styles.textStyle}>
                                    Text To Audio
                                </Text>
                            </View>,
                headerLeft: <TouchableOpacity style={styles.homeBtn}
                                onPress={()=> navigation.navigate('Mainscreen')} >
                                <Icon name="home" size={30} color={'#D3D3D3'}/>
                            </TouchableOpacity>,
                headerRight: <TouchableOpacity style={styles.drawerBtn}
                                onPress={() => navigation.toggleDrawer()} >
                                <Icon name="dots-three-vertical" size={30} color={'#D3D3D3'}/>
                            </TouchableOpacity>
            }
        }
    },
    Description: {
        screen: Description,
        navigationOptions: ({navigation}) => {
            return {
                headerStyle: {
                    backgroundColor: 'white',
                    elevation: 0
                },
                headerLeft: <TouchableOpacity style={styles.homeBtn}
                                onPress={()=> navigation.navigate('TextToAudio')} >
                                <Icon name="chevron-thin-left" size={30} color={'#D3D3D3'}/>
                            </TouchableOpacity>,
                headerRight: <TouchableOpacity style={styles.drawerBtn}
                onPress={() => navigation.toggleDrawer()} >
                <Icon name="dots-three-vertical" size={30} color={'#D3D3D3'}/>
            </TouchableOpacity>
            }
        }
    },
    TextToAudioContent: {
        screen: TextToAudioContent,
        navigationOptions: ({navigation}) => {
            return {
                headerStyle: {
                    backgroundColor: 'white',
                    elevation: 0
                },
            }
        }
    },
    AudioList: {
        screen: AudioList,
        navigationOptions: ({navigation}) => {
            return {
                headerStyle: {
                    backgroundColor: 'white',
                    elevation: 0
                },
                headerTitle:<View style={styles.titleTextView}>
                                <Text style={styles.textStyle}>
                                    Audio To Text 
                                </Text>
                            </View>,
                headerLeft: <TouchableOpacity style={styles.homeBtn}
                                onPress={()=> navigation.navigate('Mainscreen')} >
                                <Icon name="home" size={30} color={'#D3D3D3'}/>
                            </TouchableOpacity>,
                headerRight: <TouchableOpacity style={styles.drawerBtn}
                                onPress={() => navigation.toggleDrawer()} >
                                <Icon name="dots-three-vertical" size={30} color={'#D3D3D3'}/>
                            </TouchableOpacity>
            }
        }
    },
    LanguageTranslator: {
        screen: LanguageTranslator,
        navigationOptions: ({navigation}) => {
            return {
                headerStyle: {
                    backgroundColor: 'white',
                    elevation: 0
                },
                headerTitle:<View style={styles.titleTextView}>
                                <Text style={styles.textStyle}>
                                    Language Translator 
                                </Text>
                            </View>,
                headerLeft: <TouchableOpacity style={styles.homeBtn}
                                onPress={()=> navigation.navigate('Mainscreen')} >
                                <Icon name="home" size={30} color={'#D3D3D3'}/>
                            </TouchableOpacity>,
                headerRight: <TouchableOpacity style={styles.drawerBtn}
                                onPress={() => navigation.toggleDrawer()} >
                                <Icon name="dots-three-vertical" size={30} color={'#D3D3D3'}/>
                            </TouchableOpacity>
            }
        }
    },
    Flatlist: {
        screen: Flatlist,
        navigationOptions: ({navigation}) => {
            return {
                headerStyle: {
                    backgroundColor: 'white',
                    elevation: 0
                }
            }
        }
    }
},
{
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: '#0B4C84',
            elevation: 0
        }
    }
}
)

const drawerRouter = createDrawerNavigator({
    Home: stackRouter,
},
{
    contentComponent: DrawerComponent,
}
)

export default router = createAppContainer(drawerRouter)