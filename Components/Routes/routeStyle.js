import { StyleSheet, Dimensions } from 'react-native';

export default styles = StyleSheet.create({
    titleTextView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStyle: {
        textAlign: 'center',
        paddingLeft: 10,
        fontWeight: 'bold'
    },
    homeBtn: {
        paddingLeft: Dimensions.get('window').width/20
    },
    drawerBtn: {
        paddingRight: Dimensions.get('window').width/20
    },
})