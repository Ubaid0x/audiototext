import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View, StyleSheet,TouchableOpacity,Image}   from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './drawerStyles';

export default class Drawer extends Component {
    render() {
        return (
            <View style={styles.mainView}>
                <View style={styles.headView}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.closeDrawer()}}>
                        <Icon name="md-arrow-back" size={30} />
                    </TouchableOpacity>
                </View>
                <View style={styles.bodyView}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Mainscreen')} >
                        <Text style={styles.bodyText}>HOME</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AudioToText')} > 
                        <Text style={styles.bodyText}>AUDIO TO TEXT</Text>
                    </TouchableOpacity> 
                    
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('TextToAudio')} >
                        <Text style={styles.bodyText}>TEXT TO AUDIO</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('LanguageTranslator')} > 
                        <Text style={styles.bodyText}>LANGUAGE TRANSLATOR</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity> 
                        <Text style={styles.bodyText}>ABOUT APP</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity> 
                        <Text style={styles.bodyText}>RATE US</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity> 
                        <Text style={styles.bodyText}>SHARE IT</Text>
                    </TouchableOpacity>
                    <View style={styles.imageView}>
                        <Image style={styles.imageStyle} 
                            source={require('../../Assets/images/logo.png')} />
                    </View>
                </View>
            </View>
        )
    } 
}