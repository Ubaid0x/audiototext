import { StyleSheet, Dimensions } from 'react-native';
const {width, height} = Dimensions.get('window');

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: 'lightgray'
    },
    headView: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginHorizontal: width/14,
        padding: 5,
        height: height/8
        
    },
    bodyView: {
        height: height/4
    },
    bodyText: {
        // textTransform: 'uppercase',
        marginHorizontal: width/14,
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: width/14
    },
    imageView: {
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    imageStyle: {
        height: 120,
        width: 120
    }
})