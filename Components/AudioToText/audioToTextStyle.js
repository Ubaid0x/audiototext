import { StyleSheet, Dimensions } from 'react-native';

let { width, height } = Dimensions.get('window');

export default audioToTextStyle = StyleSheet.create({
    mainView: {
        height: '100%',
        width: '100%'
    },
    backgroundImageStyle: {
        flex: 1,
        width: width,
        height: height
    },
    imageView: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageStyle: {
        width: width/2,
        height: height/6,
    },
    headerStyler: {
        flex: 1,
        height: 30,
        marginTop: height/6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    micBtn: {
        height: 80,
        width: 80,
        borderRadius: 50,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40,
    },
    recordTimeView: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 10,
    },
    recordTimeText: {
        fontSize: 20, 
        fontWeight: 'bold',
    },
    footerBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        backgroundColor: '#0080ff',
    },
    footerBtnText: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
    }
})