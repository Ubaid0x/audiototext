import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, Platform, NativeModules, NativeEventEmitter } from 'react-native';
import styles from './audioToTextStyle';
import Icon from 'react-native-vector-icons/Feather';
import AudioRecorderPlayer from 'react-native-audio-recorder-player';
import Permissions from 'react-native-permissions';
import SpeechRecognizer from 'react-native-speech-recognizer';

export default class AudioToText extends Component {
  constructor() {
    super();
    this.state = {
      show: true,
      recordTime: '00:00:00'
    }
    // this.audioRecorderPlayer = new AudioRecorderPlayer();
  }
  componentDidMount() {
    Permissions.check('microphone').then(response => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      this.setState({ microphonePermission: response })
    })
  }
  UNSAFE_componentWillMount() {
    this.state = {
      result: ''
    };
    SpeechRecognizer.init(result => this.setState({ result }));
  }

  componentWillUnmount(){
    SpeechRecognizer.end();
  }
  // onStartRecord = async () => {
  //   let random = Math.floor((Math.random() * 100) + 1)
  //   const path = Platform.select({
  //     ios: 'hello.m4a',
  //     android: `sdcard/hello${random}.mp4`,
  //   });
  //   const result = await this.audioRecorderPlayer.startRecorder(path);
  //   this.setState({ show: false })
  //   this.audioRecorderPlayer.addRecordBackListener((e) => {
  //     this.setState({
  //       recordSecs: e.current_position,
  //       recordTime: this.audioRecorderPlayer.mmssss(Math.floor(e.current_position)),
  //     });
  //     return;
  //   });
  //   console.log(result);
  // }

  // onStopRecord = async () => {
  //   this.setState({ show: true });
  //   const result = await this.audioRecorderPlayer.stopRecorder();
  //   this.audioRecorderPlayer.removeRecordBackListener();
  //   this.setState({
  //     recordSecs: 0,
  //   });
  //   // console.log(result);
  // }

  render() {
    return (
      <View style={styles.mainView}>
        <ImageBackground style={styles.backgroundImageStyle}
          source={require('../../Assets/images/backgroundImage.jpeg')}
        >
          <View style={styles.imageView}>
            <Image source={require('../../Assets/images/splash_with_option.jpg')} style={styles.imageStyle} />
          </View>
          <View style={styles.headerStyler}>
            {this.state.show ?
              <TouchableOpacity style={styles.micBtn}
                onPress={this.start}>
                <Icon name="mic" size={40} />
              </TouchableOpacity> :
              <TouchableOpacity style={styles.micBtn}
                onPress={this.stop}>
                <Icon name="play" size={40} />
              </TouchableOpacity>
            }
            <View style={styles.recordTimeView}>
              <Text style={styles.recordTimeText}>{this.state.recordTime}</Text>
            </View>
          </View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('AudioList')}
            style={styles.footerBtn} >
            <Text style={styles.footerBtnText}>Convert It</Text>
          </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  }
}