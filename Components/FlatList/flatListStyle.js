import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
export default styles = StyleSheet.create({
    listView: {
        marginHorizontal: width/16,
        marginVertical: height/40,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        paddingBottom: 20
    },
    centerTextView: {
        width: width/1.8,
        paddingLeft: 15
    },
    textColor: {
        color: 'black'
    },  
    textSize: {
        fontSize: 12
    },
    convertBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#0080ff',
        height: 30,
        width: 80,
        borderRadius: 30,
    },
    convertBtnText: {
        textAlign: 'center',
        color: 'white'
    },
    editView: {
        position: 'absolute',
        zIndex: 10,
        bottom: 0,
        right: 10,
    },
})