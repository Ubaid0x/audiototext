import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import styles from './flatListStyle';

export default class Flatlist extends Component {
    renderItem({ item }) {
        return (
            <View style={styles.listView}>
                {item.time ? 
                    <Icon name="play-circle" size={40} color={'#D3D3D3'}/> :
                    <Icon name="type" size={40} color={'#D3D3D3'}/>
                }
                {item.time ? 
                    <TouchableOpacity>
                    <View style={styles.centerTextView}>
                        <Text style={styles.textColor}>{item.title}</Text>
                        <Text style={styles.textSize}>{item.time}</Text> 
                    </View>
                    </TouchableOpacity> :
                    <TouchableOpacity>
                        <View style={styles.centerTextView}>
                            <Text style={styles.textColor}>{item.title}</Text>
                            <Text style={styles.textSize}>{item.language}</Text>
                        </View>
                    </TouchableOpacity>
                }
                
                <TouchableOpacity style={styles.convertBtn}>
                    <Text style={styles.convertBtnText}>Convert It</Text>
                </TouchableOpacity>
            </View>
        )
    }
    render() {
        return (
            <ScrollView>
                <FlatList
                    data={this.props.data}
                    renderItem={this.renderItem.bind(this)}
                    keyExtractor={(item, index) => { return index.toString() }}
                />
            </ScrollView>
        )
    }
}