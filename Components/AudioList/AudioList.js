import React, { Component } from 'react';
import { Text, FlatList, View, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Flatlist from '../FlatList/FlatList';
import styles from './audioListStyle';

export default class AudioList extends Component {
    constructor() {
        super();
        this.state = {
            data: [
                {
                    title: 'sound.mp4',
                    time: '00:15:54'
                },
                {
                    title: 'sound.mp4',
                    time: '00:15:54'
                },
                {
                    title: 'sound.mp4',
                    time: '00:15:54'
                },
                {
                    title: 'sound.mp4',
                    time: '00:15:54'
                },
                {
                    title: 'sound.mp4',
                    time: '00:15:54'
                },
                {
                    title: 'sound.mp4',
                    time: '00:15:54'
                },
                {
                    title: 'sound.mp4',
                    time: '00:15:54'
                }
            ]
        }
    }
    
    render() {
        return (
            <View>
                <View style={styles.searchView}>
                    <TextInput
                        placeholder='Search text title'
                        style={styles.textInput}
                    />
                    <Icon name="search" color={'#D3D3D3'} 
                        style={styles.searchIcon} size={30} />
                </View>
                <Flatlist data={this.state.data} />
            </View>
        )
    }
}