import React, { Component } from 'react';
import { View, TextInput, Text, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import styles from './textToAudioStyle';
import Icon from 'react-native-vector-icons/Feather';


export default class TextToAudio extends Component {
    constructor() {
        super();
        this.state = {
            data: [
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                }
            ]
        }
    }

    renderItem({ item }) {
        return (
            <View style={styles.listView}>
                <Icon name="type" size={40} color={'#D3D3D3'} />
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Description')}>
                    <View style={styles.centerTextView}>
                        <Text style={styles.textColor}>{item.title}</Text>
                        <Text style={styles.textSize}>{item.language}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.convertBtn}>
                    <Text style={styles.convertBtnText}>Convert It</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.mainView}>
                <View style={styles.searchView}>
                    <TextInput
                        placeholder='Search text title'
                        style={styles.textInput} />
                    <Icon name="search" color={'#D3D3D3'}
                        style={styles.searchIcon} size={30} />
                </View>
                <TouchableOpacity style={styles.editView}>
                    <Icon name="edit" size={55} color={'#000'} />
                </TouchableOpacity>
                <ScrollView>
                <FlatList
                    data={this.state.data}
                    renderItem={this.renderItem.bind(this)}
                    keyExtractor={(item, index) => { return index.toString() }}
                />
                </ScrollView>
            </View>
        );
    }
}