import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity } from 'react-native';
import styles from './languageTranslatorStyle';
import Icon from 'react-native-vector-icons/Entypo';
import Flatlist from '../FlatList/FlatList';

export default class LanguageTranslator extends Component {
    constructor() {
        super();
        this.state = {
            data: [
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                },
                {
                    title: 'My Country History',
                    language: 'English'
                }
            ]
        }
    }

    render() {
        return (
            <View style={styles.mainView}>
                <View style={styles.searchView}>
                    <TextInput
                        placeholder='Search text title'
                        style={styles.textInput} />
                    <Icon name="traffic-cone" color={'#D3D3D3'}
                        style={styles.searchIcon} size={30} />
                </View>
                <TouchableOpacity style={styles.editView}>
                    <Icon name="circle-with-plus" size={55} color={'#2B60DE'} />
                </TouchableOpacity>
                <Flatlist data={this.state.data} />
            </View>
        )
    }
}