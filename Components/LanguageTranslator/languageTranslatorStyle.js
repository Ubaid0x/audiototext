import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        position: 'relative'
    },
    searchView: {
        flexDirection: 'row'
    },
    textInput: {
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 5,
        width: '85%',
        marginHorizontal: width/16,
        position: 'relative'
    },
    searchIcon: {
        position: 'absolute',
        right: 40,
        top: 15
    },  
    listView: {
        marginHorizontal: width/16,
        marginVertical: height/40,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        paddingBottom: 20
    },
    editView: {
        position: 'absolute',
        zIndex: 10,
        bottom: 0,
        right: 10,
    },
})