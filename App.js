import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Mainscreen from './Components/Mainscreen/Mainscreen';
import AudioToText from './Components/AudioToText/AudioToText';
import Router from './Components/Routes/Routes';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Router />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})

